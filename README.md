# XChange

A [Flask]-based application to calculate, store and display currency transactions.

## Dependencies

The **XChange** API has the following dependencies:

* [Flask]
* [Flask Restful]
* [requests]

## Installation


[Pipenv] is probably the most convenient way to set up all
dependencies, right away in a virtual environment. You only need to run the 

```
pipenv install [--dev]
```
command in the project root directory. `--dev` is recommended for
developers or those who would like to run the tests at least. 

Otherwise pre-requisites have to beinstalled manually.


## Configuration

A couple of configuration variables need to be set before we could
start up the service for the first time. 
To facilitate this task, example [.env](./.env_sample) file is provided,
with example values, typically for [Pipenv] usage.

|**Variable**             | Value                                           |   Required    | Default |
| ----------------------- |:----------------------------------------------- |:-------------:|:-------:|
| DB_PROTOCOL             | database protocol string (example: `postgres`)  | **mandatory** |         |
| DB_USER                 | database user                                   | **mandatory** |         |
| DB_PASSWORD             | database password                               | **mandatory** |         |
| DB_NAME                 | database name                                   | **mandatory** |         |
| DB_HOST                 | database host                                   | **mandatory** |         |
| DB_PORT                 | database port                                   | **mandatory** |         |
| FIXERIO_URL             | [FixerIO] API url                               | **mandatory** |         |
| FIXERIO_KEY             | [FixerIO] API access key                        | **mandatory** |         |
| FLASK_APP               | wsgi executable location used in [Development Environment](#usage) |   optional    |         |



### FixerIO

The [FixerIO] service API is used for currency rate calculations.
The service offers a free plan for up to 1000 API calls a month.

You will need to register in order to recieve an API key
for the configuration.


## Usage

Once all configuration is set, you can bring up the service
in a Development Environment with a simple

```
flask run
```
comand.

For production usage we suggest [Gunicorn]
```
gunicorn 'xchange:create_app()'
```

## XChange API 

**XChange** API provides the following two endpoints:

### Uploading new error

```
POST /xchange
```
The expected paylad is:
```
{
    "sell_currency": "EUR",
    "buy_currency": "HUF",
    "amount": 200,
}

```
The `amount` field refers to the amount sold.

On success, the response contains the newly created entry, with a number of 
additional, generated fields:
```
{
    "transaction_id": "TRNJF1F0A",
    "sell_currency": "EUR",
    "buy_currency": "HUF",
    "sell_amount": 200.0,
    "buy_amount": 65875.8266,
    "rate": 329.379133,
    "date_booked": "2019-11-05T01:20:01"
}

```


### Listing transactions

```
GET /xchange
```
With no parameters, GET queries return the last 50 transactions.

The response format is 
```
{
    "transactions": [
        {
            "transaction_id": "TRNJF1F0A",
            "sell_currency": "EUR",
            "buy_currency": "HUF",
            "sell_amount": 200.0,
            "buy_amount": 65875.8,
            "rate": 329.379,
            "date_booked": "2019-11-05T01:20:01"
        },
    [..]
}
```
With the `items` parameter, we can limit the result set to our choice.
```
{
    "items": 200
}
```
(The output has the same structure, as above.)

Using the `transaction_id` parameter, 
```
{
    "transaction_id": "TRNJF1F0A"
}
```
the API returns the corresponding transaction item:
```
{
    "transactions": {
        "transaction_id": "TRNJF1F0A",
        "sell_currency": "EUR",
        "buy_currency": "HUF",
        "sell_amount": 200.0,
        "buy_amount": 65875.8,
        "rate": 329.379,
        "date_booked": "2019-11-05T01:20:01"
    }
}

```



## XChange Frontend

There are two views provided by the application. The main page
is listing the last 50 transactions

<table border=1 width=80%><tr><td>
![Main view](docs/xchange-main.png)
</td></tr></table>

Using the `New transaction` button the user is redirected to the
second view, which allows to add a new transaction.

<table border=1 width=80%><tr><td>
![New transaction](docs/xchange-new.png)
</td></tr></table>

## Docker

The **XChange** code base also includes support for Docker.

### Docker Image

The [Dockerfile] for the **XChange** API is simple and straightforward.

The image can be built with the following command (launched from the
project root, i.e. the location of the [Dockerfile]).
```
docker build -t xchange:latest .
```
## Docker Services

**XChange** as a service can be started up using the
[docker-compose.yml] file, including all dependencies.

In this setup, **XChange** dependencies were chosen as

* database: [mysql](https://hub.docker.com/_/mysql)

### Setup

A number of configuration settings must be defined before the services
could run.

The [.env] file used for [Pipenv] environment variables
is also a valid input for `docker-compose`. 
If you weren't using [Pipenv], and don't have a [.env] file yet, then
we suggest you to start using it now, with the list of
variables below. 

**NOTE**: Make sure that there are **NO QUOTES** around string
values in your [.env] file. Parameters are only passed correctly
to Docker without quotes. (Quotes can result in failures that aren't
easy to debug.)


|**Variable**             | Value                |
| ----------------------- |:---------------------|
| DB_USER                 | database user        |
| DB_PASSWORD             | database password    |
| DB_NAME                 | database name        |
| FIXERIO_URL             | [FixerIO] API url    | 
| FIXERIO_KEY             | [FixerIO] token      | 

* `DB_USER`, `DB_PASSWORD`, `DB_NAME`: arbitraty choice
    *  will be set both for the database container and the **XChange** API container.

* `FIXERIO_URL`, `FIXERIO_KEY`: the API url and valid token for [FixerIO]


### Usage

Once the `xchange:latest` image is [built](#docker-image),
default usage is quite simple:
```
docker-compose up
```
You should find a  **XChange** API instance listening on:
```
http://<hostname>/xchange
```

## Internals

The API is based on the [Flask] web framework, using the [Flask Restful] extension. The latter provides
a number of helpful features, like error handling or input validation.

The API by default is using a MySQL database backend. Likely to work with other
databases as well, thanks to always accessing the database via the 
Python [sqlalchemy] ORM. (Also tested against SQLite).

The code is written in Python 3.7.

The front-end is using Flask blueprints, with Jinja2 and JavaScript.


[.env]: ./.env_sample
[docker-compose.yml]: ./docker-compose.yml
[Dockerfile]: ./Dockerfile
[FixerIO]: http://fixer.io
[Flask]: http://flask.palletsprojects.com/en/1.1.x/
[Flask Restful]: https://flask-restplus.readthedocs.io/en/stable/index.html
[Gunicorn]: https://gunicorn.org/
[Pipenv]: https://pipenv-fork.readthedocs.io/en/latest/
[Python lru_cache]: https://docs.python.org/3/library/functools.html#functools.lru_cache
[requests]: https://requests.kennethreitz.org/en/master/
[sqlalchemy]: https://www.sqlalchemy.org
