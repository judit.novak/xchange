import json

from xchange.helpers import round_up_decimal
from xchange.models import Transaction


# Helper function
def last_transaction(app):
    with app.app_context():
        return Transaction.query.order_by(
            Transaction.transaction_id.desc()
        ).first()


def test_allowed_methods(client):
    assert client.get('/api/xchange').status_code == 200
    assert client.put('/api/xchange').status_code == 405
    resp = client.post(
        '/api/xchange',
        content_type='application/json',
        data=json.dumps({
            'sell_currency': 'EUR',
            'buy_currency': 'USD',
            'amount': 20
        })
    )
    assert resp.status_code == 200


def test_xchange_post_missing_parameters(client):
    resp = client.post('/api/xchange')
    assert resp.status_code == 400
    assert resp.json['message'] == {
        "sell_currency": "ISO3 currency code",
    }


def test_xchange_post_success(app, client, rates):
    resp = client.post(
        '/api/xchange',
        content_type='application/json',
        data=json.dumps({
            'sell_currency': 'EUR',
            'buy_currency': 'USD',
            'amount': 20
        })
    )
    resp_data = dict(resp.json)
    resp_data.pop('date_booked')
    tr_id = resp_data.pop('transaction_id')
    assert resp_data == {
        'sell_currency': 'EUR',
        'buy_currency': 'USD',
        'sell_amount': 20.0,
        'rate': rates['USD'],
        'buy_amount': round_up_decimal(20 * rates['USD'])
    }

    with app.app_context():
        row = Transaction.query.filter(
            Transaction.transaction_id == tr_id
        ).first()
    assert row.buy_currency == 'USD'
    assert row.buy_amount == 22.336


def test_error_handler(app, client, rates):
    resp = client.post(
        '/api/xchange',
        content_type='application/json',
        data=json.dumps({
            'sell_currency': '???',
            'buy_currency': 'USD',
            'amount': 20
        })
    )
    resp_data = dict(resp.json)
    assert resp.status_code == 500
    assert resp_data['message'] == ("Couldn't retrieve exchange "
                                    "rate for '???' to 'USD'")


def test_get(app, client, rates):
    client.post(
        '/api/xchange',
        content_type='application/json',
        data=json.dumps({
            'sell_currency': 'EUR',
            'buy_currency': 'USD',
            'amount': 20
        })
    )
    client.post(
        '/api/xchange',
        content_type='application/json',
        data=json.dumps({
            'sell_currency': 'HUF',
            'buy_currency': 'USD',
            'amount': 20000
        })
    )

    resp = client.get('/api/xchange')
    resp_data = resp.json
    assert len(resp_data['transactions']) == 2

    tr_id = resp_data['transactions'][0]['transaction_id']
    resp = client.get('/api/xchange',
                      data={'transaction_id': tr_id})
    resp_data = resp.json
    assert isinstance(resp_data['transactions'], dict)
    assert resp_data['transactions']['transaction_id'] == tr_id

    # Latest first
    resp = client.get('/api/xchange',
                      data={'items': 1})
    resp_data = resp.json
    assert len(resp_data['transactions']) == 1
    assert resp_data['transactions'][0]['transaction_id'] == tr_id

    resp = client.get('/api/xchange',
                      data={'items': 2})
    resp_data = resp.json
    assert len(resp_data['transactions']) == 2
