import mock

from xchange.db_functions import get_transaction
from xchange.db_functions import add_transaction
from xchange.models import Transaction


def test_add_transaction(app):
    with app.app_context():
        tr = add_transaction('EUR', 'USD', 20, 22.34, 1.1168)
    assert isinstance(tr, Transaction)


@mock.patch('xchange.db_functions.generate_trasaction_id',
            side_effect=['TR1234567', 'TR1234567', 'TRIJFHKES'])
def test_add_transaction_integrity_error(mock_generate_id, app):
    with app.app_context():
        tr1 = add_transaction('EUR', 'USD', 20, 22.34, 1.1168)
        tr2 = add_transaction('EUR', 'USD', 20, 22.34, 1.1168)
        assert tr1.transaction_id == 'TR1234567'
        assert tr2.transaction_id == 'TRIJFHKES'


def test_get_transaction(app):
    with app.app_context():
        tr = add_transaction('EUR', 'USD', 20, 22.34, 1.1168)
        tr2 = add_transaction('EUR', 'HUF', 500, 329.316317, 164658.1585)

        rows = get_transaction()
        assert rows == [tr2, tr]

        rows = get_transaction(tr.transaction_id)
        assert rows == tr

        rows = get_transaction(items=1)
        assert rows == [tr2]
