import pytest

from xchange import create_app
from xchange.exceptions import XChangeException


class TestConfig:
    SQLALCHEMY_DATABASE_URI = 'sqlite:///:memory:'
    FIXERIO_KEY = 'blah'
    FIXERIO_URL = 'http://data.fixer.io/api/latest'


rates_data = {
    'EUR': 1,
    'GBP': 0.818142,
    'HUF': 296.621603,
    'USD': 1.1168
}


@pytest.fixture
def rates():
    return rates_data


def text_callback(request, context):
    # Return all rates if no 'symbols' parameter
    if not request.qs.get('symbols'):
        return {
            "success": "true",
            "rates": rates_data
        }

    # Return 'base', 'target'
    sell_cur, buy_cur = request.qs['symbols'][0].upper().split(',')
    if not (sell_cur in rates_data
            and buy_cur in rates_data):
        raise XChangeException(
            f"Couldn't retrieve exchange rate for '{sell_cur}' to '{buy_cur}'"
        )
    return {
        "success": "true",
        "rates": {
            sell_cur: rates_data[sell_cur],
            buy_cur: rates_data[buy_cur]
        }
    }


@pytest.fixture
def app(requests_mock):
    requests_mock.get(
            'http://data.fixer.io/api/latest?access_key=blah',
            json=text_callback)
    return create_app(config=TestConfig)


@pytest.fixture
def client(app, requests_mock):
    return app.test_client()
