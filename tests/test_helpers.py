import pytest
from requests.exceptions import HTTPError
from string import digits, ascii_uppercase

from xchange.exceptions import XChangeException
from xchange.helpers import generate_id
from xchange.helpers import generate_trasaction_id
from xchange.helpers import get_rate
from xchange.helpers import get_currencies
from xchange.helpers import contact_fixerio
from xchange.helpers import round_up_decimal


def test_contact_fixerio(app, rates):
    with app.app_context():
        assert contact_fixerio() == rates

        fx_rates = contact_fixerio(params={'symbols': ['HUF', 'EUR']})
        assert fx_rates == {'HUF': rates['HUF'], 'EUR': rates['EUR']}


def test_fixerio_bad_response(app, requests_mock):

    url = 'http://data.fixer.io/api/latest'

    with app.app_context():

        requests_mock.get(url, text='Bad request', status_code=400)
        with pytest.raises(XChangeException) as err:
            assert contact_fixerio()
        assert str(err.value) == f"Error returned from {url}: 'Bad request'"

        requests_mock.get(url, exc=HTTPError)
        with pytest.raises(XChangeException) as err:
            assert contact_fixerio()
        assert str(err.value) == f"Couldn't contact {url}: ''"

        requests_mock.get(url, json={})
        with pytest.raises(XChangeException) as err:
            assert contact_fixerio()
        assert str(err.value) == f"No JSON returned from {url}"

        requests_mock.get(url, json={'success': False})
        with pytest.raises(XChangeException) as err:
            assert contact_fixerio()
        assert str(err.value) == f"Failed to retrieve data from {url}"


def test_get_currencies(app, rates):
    with app.app_context():
        assert get_currencies() == list(rates.keys())


def test_get_rate(app, rates):
    with app.app_context():
        rate = get_rate('EUR', 'HUF')
        assert rate == 296.621603

        with pytest.raises(XChangeException) as err:
            get_rate('MISSING CURRENCY', 'EUR')
        assert str(err.value) == ("Couldn't retrieve exchange rate for "
                                  "'MISSING CURRENCY' to 'EUR'")


def test_round_up_decimal():
    assert round_up_decimal(.24) == .24
    assert round_up_decimal(.123456789) == .12345679
    assert round_up_decimal(123456789) == 123456789
    assert round_up_decimal(1111.123456781, 6) == 1111.123457
    assert round_up_decimal(1111.12345678001, 2) == 1111.13


def test_generate_id():

    assert generate_id() != generate_id()

    identifier = generate_id()
    assert set(identifier) < set(digits + ascii_uppercase)
    assert len(identifier) == 7

    assert len(generate_id(8)) == 8


def test_generate_transaction_id():
    trans_id = generate_trasaction_id()
    assert 'TR' == trans_id[0:2]
    assert len(trans_id) == 9
    assert generate_trasaction_id() != generate_trasaction_id()
