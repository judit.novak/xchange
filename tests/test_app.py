import pytest

from sqlalchemy.exc import ArgumentError

from xchange import create_app


def test_config():
    with pytest.raises(ArgumentError):
        class BadConfig:
            SQLALCHEMY_DATABASE_URI = ""
        create_app(config=BadConfig)
