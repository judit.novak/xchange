import datetime

from . import db


class Transaction(db.Model):
    transaction_id = db.Column(db.String(9), primary_key=True, nullable=False)
    sell_currency = db.Column(db.String(3), nullable=False)
    buy_currency = db.Column(db.String(3), nullable=False)
    sell_amount = db.Column(db.Float(8), nullable=False)
    buy_amount = db.Column(db.Float(8), nullable=False)
    rate = db.Column(db.Float(8), nullable=False)
    date_booked = db.Column(db.DateTime, default=datetime.datetime.utcnow)

    def __repr__(self):
        return '<Transaction %r>' % self.transaction_id
