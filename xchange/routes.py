from flask import current_app
from flask_restful import Api


def add_routes():

    from .resources import XChange
    api = Api(current_app._get_current_object())
    api.add_resource(XChange, '/api/xchange')
