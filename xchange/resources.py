import logging

from flask_restful import fields
from flask_restful import marshal_with
from flask_restful import Resource
from flask_restful import reqparse

from .db_functions import add_transaction
from .db_functions import get_transaction
from .helpers import get_rate
from .helpers import round_up_decimal


logger = logging.getLogger(__name__)


get_resp_fields = {
    'transaction_id': fields.String,
    'sell_currency': fields.String,
    'buy_currency': fields.String,
    'sell_amount': fields.Float,
    'buy_amount': fields.Float,
    'rate': fields.Float,
    'date_booked': fields.DateTime('iso8601')
}

post_resp_fields = {
    'transaction_id': fields.String,
    'sell_currency': fields.String,
    'buy_currency': fields.String,
    'sell_amount': fields.Float,
    'buy_amount': fields.Float,
    'rate': fields.Float,
    'date_booked': fields.DateTime('iso8601')
}


class XChange(Resource):
    @marshal_with(post_resp_fields)
    def post(self):

        # Parsing parameters
        parser = reqparse.RequestParser()
        parser.add_argument(
            'sell_currency',
            type=str,
            required=True,
            help='ISO3 currency code'
        )
        parser.add_argument(
            'buy_currency',
            type=str,
            required=True,
            help='ISO3 currency code'
        )
        parser.add_argument(
            'amount',
            type=float,
            required=True,
            help='Transfer amount (int/float)'
        )
        args = parser.parse_args()

        rate = get_rate(args['sell_currency'], args['buy_currency'])

        buy_amount = round_up_decimal(args['amount'] * rate)

        transaction = add_transaction(
            args['sell_currency'],
            args['buy_currency'],
            args['amount'],
            buy_amount,
            rate,
        )

        return {
            'transaction_id': transaction.transaction_id,
            'sell_currency': args['sell_currency'],
            'buy_currency': args['buy_currency'],
            'sell_amount': args['amount'],
            'buy_amount': buy_amount,
            'rate': rate,
            'date_booked': transaction.date_booked,
        }

    @marshal_with(get_resp_fields, envelope='transactions')
    def get(self):
        parser = reqparse.RequestParser()
        parser.add_argument(
            'transaction_id',
            type=str,
            help='Transaction id'
        )
        parser.add_argument(
            'items',
            type=int,
            help='Number of entries to retrieve'
        )
        args = parser.parse_args()
        args_to_pass = {k: v for k, v in args.items() if v}
        transactions = get_transaction(**args_to_pass)
        return transactions
