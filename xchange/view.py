from flask import Blueprint
from flask import render_template

from .helpers import get_currencies


# from flaskr.db import get_db

bp = Blueprint('view', __name__, url_prefix='/')


@bp.route('/xchange')
def all_transactions():
    from .models import Transaction
    transactions = Transaction.query.all()
    return render_template('index.html',
                           transactions=transactions)


@bp.route('/xchange/submit_transaction')
def submit_transaction():
    currencies = get_currencies()
    return render_template('submit_transaction.html',
                           currencies=currencies)
