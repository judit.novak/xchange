
function numFormat (num) {
    if (isNaN(num)) {
        return ""
    }
    numstr = (num).toFixed(8).replace(/\d(?=(\d{3})+\.)/g, '$&,');
    return numstr.replace(/\.?0+$/, '')
}

