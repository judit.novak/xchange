import math
from flask import current_app
from functools import lru_cache
import requests
from random import choice
from requests.exceptions import RequestException
from string import digits, ascii_uppercase


from .exceptions import XChangeException
from .exceptions import FixerIOException


def contact_fixerio(params=None):

    ext_params = {
        'access_key': current_app.config.get('FIXERIO_KEY')
    }

    if params:
        # We need to convert it manually to a comma-separated string
        # requests would send it as separate, repetitive parameters,
        # (&symbols=<S1>&symbols=<S2>) ignored by fixerio
        symbols_arr = params.pop('symbols')
        if symbols_arr:
            ext_params['symbols'] = ','.join(symbols_arr)
        ext_params.update(params)

    fx_url = current_app.config.get('FIXERIO_URL')
    resp = None
    try:
        resp = requests.get(fx_url, params=ext_params)
    except RequestException as err:
        raise FixerIOException(f"Couldn't contact {fx_url}: '{str(err)}'")

    if resp.status_code != 200:
        raise FixerIOException(f"Error returned from {fx_url}: '{resp.text}'")

    if not resp.json():
        raise FixerIOException(f"No JSON returned from {fx_url}")

    resp_data = resp.json()

    if not resp_data['success']:
        raise FixerIOException(f"Failed to retrieve data from {fx_url}")
    return resp_data.get('rates')


@lru_cache()
def get_currencies():
    resp_data = contact_fixerio()
    return list(resp_data.keys())


def get_rate(base, target):
    params = {'symbols': [base, target]}
    resp_data = contact_fixerio(params)
    rate_base = resp_data.get(base)
    rate_target = resp_data.get(target)
    if not (rate_base and rate_target
            and isinstance(rate_base, (float, int))
            and isinstance(rate_target, (float, int))
            ):
        raise XChangeException(
            f"Couldn't retrieve exchange rate for '{base}' to '{target}'"
        )
    return (1.0/float(rate_base)) * float(rate_target)


def round_up_decimal(value, decimal=8):
    digits = 10 ** decimal
    value = value * digits
    # Always round up
    value = math.ceil(value)
    return value / digits


def generate_id(length=7):
    chars = digits + ascii_uppercase
    return "".join([choice(chars) for i in range(length)])


def generate_trasaction_id():
    return 'TR' + generate_id()
