import logging

from flask import jsonify
from werkzeug.exceptions import HTTPException

logger = logging.getLogger(__name__)


class XChangeException(Exception):
    pass


class FixerIOException(XChangeException):
    pass


class DBException(XChangeException):
    pass


def xchange_handler(error):
    try:
        message = error.message
    except AttributeError:
        message = str(error)
    logger.error(f"[ERROR]: {message}")
    response = jsonify(message=message)
    response.status_code = (
        error.code if isinstance(error, HTTPException) else 500
    )
    return response
