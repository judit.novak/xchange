from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm.exc import FlushError

from .models import Transaction
from . import db
from .helpers import generate_trasaction_id
from .exceptions import DBException


def add_transaction(sell_currency,
                    buy_currency,
                    sell_amount,
                    buy_amount,
                    rate):

    transaction_id = generate_trasaction_id()
    while get_transaction(transaction_id):
        transaction_id = generate_trasaction_id()

    try:
        transaction = Transaction(
            transaction_id=transaction_id,
            sell_currency=sell_currency,
            buy_currency=buy_currency,
            sell_amount=sell_amount,
            buy_amount=buy_amount,
            rate=rate,
        )
        db.session.add(transaction)
        db.session.commit()
    except (IntegrityError, FlushError):
        # This may on highly unlucky occasions
        # when running multiple instances of the API
        db.session.rollback()
        raise DBException(
            f"Couldn't register transaction {transaction_id}, {sell_amount}, "
            f"{sell_amount}, {buy_currency}"
        )
    return transaction


def get_transaction(transaction_id=None, items=50):
    res = None
    if not items:
        return
    if transaction_id:
        res = Transaction.query.filter(
                Transaction.transaction_id == transaction_id
        ).first()
    else:
        res = Transaction.query.order_by(
                Transaction.date_booked.desc()
              ).limit(items).all()
    return res
