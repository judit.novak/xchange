from flask import Flask
from flask_sqlalchemy import SQLAlchemy

from .exceptions import XChangeException
from .exceptions import xchange_handler
from .routes import add_routes


db = SQLAlchemy()


def create_app(config='config.Config'):
    """Construct the core application."""
    app = Flask(__name__, instance_relative_config=False)
    app.config.from_object(config)
    db.init_app(app)

    with app.app_context():
        from . import models    # noqa

        # Create tables for our models
        db.create_all()

        add_routes()

        app.register_error_handler(XChangeException, xchange_handler)

        from . import view
        app.register_blueprint(view.bp)

        return app
