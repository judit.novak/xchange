FROM python:3.7

MAINTAINER Judit Novak "judit.novak@gmail.com"

RUN apt update -y && \
    apt install -y python-pip python-dev default-libmysqlclient-dev

COPY ./requirements.txt /app/requirements.txt

WORKDIR /app

RUN pip install -r requirements.txt

COPY . /app

ENTRYPOINT [ "gunicorn" ]

CMD [ "-b", "0.0.0.0:8000", "--access-logfile", "-", "xchange:create_app()" ]
